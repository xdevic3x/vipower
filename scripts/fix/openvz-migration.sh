#!/bin/bash
IP=""
for CT in $(vzlist -H -o veid); do
echo "START $CT $(date)" >> /root/openvz-migration.log
vzmigrate --remove-area no --keep-dst --live $IP $CT >> /root/openvz-status.log 2>&1
echo "STOP $CT $(date)" >> /root/openvz-migration.log
echo "DONE $CT $(date)" >> /root/openvz-migrationdone.log
done