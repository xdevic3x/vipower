#!/bin/bash
VERSION="1.4"

##autoupdate
GLOBAL_VERSION=$(curl -s https://bitbucket.org/vibiznes/vipower/raw/master/firewall.sh | grep ^VERSION | cut -d = -f 2 | awk '{print substr($0, 2, length($0) - 2)}')
if [ $VERSION == $GLOBAL_VERSION ]; then
echo "Backup software is up to date"
else
echo "Backup software will update"
curl -s https://bitbucket.org/vibiznes/vipower/raw/master/firewall.sh -o /root/firewall.sh
chmod +x /root/firewall.sh
fi

#Limit each container IP to 10k pps udp
/sbin/iptables --flush FORWARD
for d in $(/usr/sbin/vzlist -H -a -o ip)
do
WYJATEK=$(cat /root/firewall.exc |grep '^'$d'$')
if [ -z $WYJATEK ]; then
ipv4=$(ipcalc -c4 $d)
if [ -z $ipv4 ]; then
/sbin/iptables -I FORWARD 1 -p udp -s $d -m state --state NEW -j ACCEPT
/sbin/iptables -I FORWARD 2 -p udp -d $d -m state --state NEW -j ACCEPT
/sbin/iptables -I FORWARD 3 -s $d -d 208.67.222.222 -j ACCEPT
/sbin/iptables -I FORWARD 4 -s $d -d 208.67.220.220 -j ACCEPT
/sbin/iptables -I FORWARD 5 -d $d -s 208.67.222.222 -j ACCEPT
/sbin/iptables -I FORWARD 6 -d $d -s 208.67.220.220 -j ACCEPT
#CS&TS PORT START
/sbin/iptables -I FORWARD 7 -p udp -s $d --dport 9987 -j ACCEPT
/sbin/iptables -I FORWARD 8 -p udp -s $d --dport 2010:2110 -j ACCEPT
/sbin/iptables -I FORWARD 9 -p udp -s $d --dport 27000:27050 -j ACCEPT
/sbin/iptables -I FORWARD 10 -p udp -s $d --sport 9987 -j ACCEPT
/sbin/iptables -I FORWARD 11 -p udp -s $d --sport 2010:2110 -j ACCEPT
/sbin/iptables -I FORWARD 12 -p udp -s $d --sport 27000:27050 -j ACCEPT
/sbin/iptables -I FORWARD 13 -p udp -d $d --dport 9987 -j ACCEPT
/sbin/iptables -I FORWARD 14 -p udp -d $d --dport 2010:2110 -j ACCEPT
/sbin/iptables -I FORWARD 15 -p udp -d $d --dport 27000:27050 -j ACCEPT
/sbin/iptables -I FORWARD 16 -p udp -d $d --sport 9987 -j ACCEPT
/sbin/iptables -I FORWARD 17 -p udp -d $d --sport 2010:2110 -j ACCEPT
/sbin/iptables -I FORWARD 18 -p udp -d $d --sport 27000:27050 -j ACCEPT
#CS&TS PORT END
#BIND PORT START
/sbin/iptables -I FORWARD 19 -p udp -s $d --dport 53 -j ACCEPT
/sbin/iptables -I FORWARD 20 -p udp -s $d --sport 53 -j ACCEPT
/sbin/iptables -I FORWARD 21 -p udp -d $d --dport 53 -j ACCEPT
/sbin/iptables -I FORWARD 22 -p udp -d $d --sport 53 -j ACCEPT
#BIND PORT END
#MEMCACHED DROP	START
iptables -I FORWARD 23 -d $d -p udp --dport 11211 -j DROP
iptables -I FORWARD 24 -d $d -p tcp --dport 11211 -j DROP
#MEMCACHED DROP	END

/sbin/iptables -I FORWARD 25 -p udp -s $d -m limit --limit 10000/sec --limit-burst 50 -j ACCEPT
/sbin/iptables -I FORWARD 26 -p udp -d $d -m limit --limit 10000/sec --limit-burst 50 -j ACCEPT
/sbin/iptables -I FORWARD 27 -p udp -s $d -j REJECT
fi
else
echo $d : Limit pps na firewallu zdjety
fi
done

if [ ! -f /root/smtp_limit.list ]; then touch /root/smtp_limit.list; fi
for ip in $(cat /root/smtp_limit.list); do
 echo "Limituję: $ip; dla wychodzącego 25/tcp"
 /sbin/iptables -I FORWARD -p tcp -s $ip --dport 25 -j DROP
 /sbin/iptables -I FORWARD -p tcp -s $ip --dport 25 -j LOG
 /sbin/iptables -I FORWARD -p tcp -s $ip --dport 25 -m limit --limit 42/min -j ACCEPT
done
