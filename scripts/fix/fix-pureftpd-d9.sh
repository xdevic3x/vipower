#!/bin/bash

rm -rf /var/tmp/fix-pureftpd/
mkdir -p /var/tmp/fix-pureftpd
wget https://bitbucket.org/vibiznes/vipower/downloads/pure-ftpd-common_1.0.43-3_all.deb -O /var/tmp/fix-pureftpd/pure-ftpd-common_1.0.43-3_all.deb
wget https://bitbucket.org/vibiznes/vipower/downloads/pure-ftpd-mysql_1.0.43-3_amd64.deb -O /var/tmp/fix-pureftpd/pure-ftpd-mysql_1.0.43-3_amd64.deb
dpkg -i /var/tmp/fix-pureftpd/pure-ftpd-common_1.0.43-3_all.deb
dpkg -i /var/tmp/fix-pureftpd/pure-ftpd-mysql_1.0.43-3_amd64.deb
rm -rf /var/tmp/fix-pureftpd/
