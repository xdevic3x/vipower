#!/bin/bash
VERSION="3.0"
PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
SSH=$1
HOST=$(cat /root/scripts/hostname)
ADDRESS="alert@nodea.pl"

#BORG VARIABLES
BORGBACKUPDIR="borg"
REPOSITORY=$SSH:$BORGBACKUPDIR
BORG=$(which borg)
KEEP_DAILY="7"
KEEP_WEEKLY="4"
KEEP_MONTHLY="3"
BACKUP_DIRS="/home \
        /etc \
        /var/www \
        /root \
        /var/vmail \
        /var/mail \
        /srv \
        /var/spool/cron \
        --exclude '/home/system' \
        --exclude '/home/*/admin_backups' \
        --exclude '/home/*/backups' \
        --exclude '/home/*/user_backups' \
        --exclude '/home/*/.softaculous' \
        --exclude '/home/tmp' \
        --exclude '/home/mysql'"


#MySQL-SSH-BACKUP
BACKUPDIR="~/mysql"
BACKUPLOG="/tmp/mysql-tmp.log"
OUTPUT="/var/tmp/mysql-backup"
DATA=$(date +%Y-%m-%d--%H-%M)
SSHBIN=$(which ssh)
MYSQLDUMPBIN=$(which mysqldump)
MYSQLBIN=$(which mysql)
SCPBIN=$(which scp)
php=$(which php)
BACKUPTIME="7"

function send_mail {

if [ $status = "OK" ]; then
echo "OK" > /tmp/borg.status
elif [ $status = "FAIL" ]; then
echo "FAIL" > /tmp/borg.status
fi

curl -XPOST \
  https://api.sparkpost.com/api/v1/transmissions \
  -H "Authorization: f1970921d671eb6ca4ddda797f0ed4fe7ee5791c" \
  -H "Content-Type: application/json" \
  -d '{"options": {"sandbox": false}, "content": {"from": "backup-alert@nodea.pl", "subject": "'"$subject"'", "text":"'"$message"'"}, "recipients": [{"address": "'"$ADDRESS"'"}]}'
}

if [ -f "/root/scripts/config-backup.sh" ]
then
source /root/scripts/config-backup.sh
fi
