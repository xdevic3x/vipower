#!/bin/bash
source /root/scripts/variables-backup.sh

##autoupdate
GLOBAL_VERSION=$(curl -s https://bitbucket.org/vibiznes/vipower/raw/master/variables-backup.sh | grep VERSION | cut -d = -f 2 | awk '{print substr($0, 2, length($0) - 2)}')
if [ $VERSION == $GLOBAL_VERSION ]; then
    echo "Backup software is up to date"
else
    echo "Backup software will update"
    echo > /tmp/backup-update.log
    curl -s https://bitbucket.org/vibiznes/vipower/raw/master/backup-install.sh | bash >> /tmp/backup-update.log 2>&1 \
    && subject="Backup UPDATE - OK '$HOST'" message=$($php -r 'echo json_encode(file_get_contents("/tmp/backup-update.log"));' | awk '{print substr($0, 2, length($0) - 2)}') send_mail \
    || subject="Backup UPDATE - FAILED '$HOST'" message=$($php -r 'echo json_encode(file_get_contents("/tmp/backup-update.log"));' | awk '{print substr($0, 2, length($0) - 2)}') send_mail
    echo > /tmp/backup-update.log
fi

#dummy initial ssh connection
$SSHBIN -oStrictHostKeyChecking=no $SSH "exit"

if [ ! -f "/var/www" ]; then
    mkdir -p /var/www
fi

if [ ! -f "/var/vmail" ]; then
    mkdir -p /var/vmail
fi

for d in $(mount |grep borgfs |awk '{print $3}'); do umount $d; done; sleep 60
echo > /tmp/borg-tmp.log
echo > /tmp/borg-prune-tmp.log

if [ -f "/root/scripts/borg-backup-pre.sh" ]
then
    bash /root/scripts/borg-backup-pre.sh $1
fi

$BORG create -v --stats                          \
        $REPOSITORY::$(hostname)-$DATA    \
        $BACKUP_DIRS >> /tmp/borg-tmp.log 2>&1 \
        && status="OK" subject="BORG Backup - OK '$HOST'" message=$($php -r 'echo json_encode(file_get_contents("/tmp/borg-tmp.log"));' | awk '{print substr($0, 2, length($0) - 2)}') send_mail \
        || status="FAIL" subject="BORG Backup - FAIL '$HOST'" message=$($php -r 'echo json_encode(file_get_contents("/tmp/borg-tmp.log"));' | awk '{print substr($0, 2, length($0) - 2)}') send_mail
echo > /tmp/borg-tmp.log

$BORG prune -v $REPOSITORY \
    --keep-daily=$KEEP_DAILY --keep-weekly=$KEEP_WEEKLY --keep-monthly=$KEEP_MONTHLY >> /tmp/borg-prune-tmp.log 2>&1 \
    && subject="BORG PRUNE - OK '$HOST'" message=$($php -r 'echo json_encode(file_get_contents("/tmp/borg-prune-tmp.log"));' | awk '{print substr($0, 2, length($0) - 2)}') send_mail \
    || subject="BORG PRUNE - FAIL '$HOST'" message=$($php -r 'echo json_encode(file_get_contents("/tmp/borg-prune-tmp.log"));' | awk '{print substr($0, 2, length($0) - 2)}') send_mail
echo > /tmp/borg-prune-tmp.log

if [ -f "/root/scripts/backup-list.sh" ]
then
    bash /root/scripts/backup-list.sh $1
fi

if [ -f "/root/scripts/borg-backup-post.sh" ]
then
    bash /root/scripts/borg-backup-post.sh $1
fi
