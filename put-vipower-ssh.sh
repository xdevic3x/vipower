#!/bin/bash

SSH_DIR=~/.ssh
if ! test -d $SSH_DIR; then
    mkdir $SSH_DIR
fi
chmod 700 $SSH_DIR

sed -i  '/Administrator@vibiznes.pl/ d' ~/.ssh/authorized_keys*
sed -i  '/rsa-key-20140408/ d' ~/.ssh/authorized_keys*
sed -i  '/rsa-key-20101124/ d' ~/.ssh/authorized_keys*
sed -i  '/admin@vipower.pl/ d' ~/.ssh/authorized_keys*
sed -i '/madach@twojadmin.ninja/ d' ~/.ssh/authorized_keys*
sed -i '/p.jarosz@vipower.pl/ d' ~/.ssh/authorized_keys*
sed -i  '/fstolarski@vipower.pl/ d' ~/.ssh/authorized_keys*

SSH_RSA_KEY_MADACH="ssh-rsa AAAAB3NzaC1yc2EAAAABJQAAAgEAw5YuHqnPuef7YirNgLORaXgx2DD0fhqPiEAjgUMFuLc3e7Degu1ZE0FhP0I+iixmO5K8+8u8GlRwEEG++YQIn4XLo9/y7J1E4Pmc5BudFb1mZOyE4DosNU0iriw4Nv8LRCL6y4eKfe5OcfL7vARbiGt5sefFhcyyxYPg5Smf4h35PuX4P7ooxunTSQVNOvA8dpJ+OMO2P7aMUbllY1MKbBU21zRisbKpbp0QRxy40U24VuJHMOM4VzvOX1mzi0NEedzi7QXV6cZZAPWcnp1/X0mG5Trt1h5fU7wdCNfXxXOU1MBG6E2r1kFS7CWg0AljM1skHFFikhOuwTamLehnwNcyh+hXcw9oSf3bpLf3LWSU9f7cHTPVN8ai7tR4sGO319mEcPbrdq3VkN0XDR2Sk5Tjd4qlKjp0z7DO39A7AVoPB2mGy4xqmrfn+461HU+OpkwJhOBal/xG0cFCLHyqzrIr+z6gbpwotkxn1vUz36sfEhZ0pWrVbM3K99Nft7AvuVIy1a1iAlmNFN55RL0j/jDlaAdCtiKd9bUOPwoi2bln+Ac3Yc+geycivjW2Z+NWAO3Pav4Pzbi045voMBZH6ivu/Ki+awr+eSWYhThR0HEf6obLYrDGIMqMWEtV0r93Xu7m//9Q9OWKI70q6givPJ0MBZvHqywgpI1ug9IbbzU= madach@twojadmin.ninja"
SSH_RSA_KEY_PJAROSZ="ssh-rsa AAAAB3NzaC1yc2EAAAABJQAAAgEAtMKotfWi1Gclv74/vqxXAab+QEx+zftmgilhqICUTDkAhTbbV3coDadJwySzrNY5P5vOzxMypwHwFrCfwK9y/DTvlNb5thMBwJtQfEh1j72yqyA4bcMl2mandltlDJZNQs8Fgsy2+Yoipn+xGi/0H2pCFX5crF4wb0eV+uUIQpNuUfFh0Azdvlax7ZtNMBA9jWyIU2+sjkbXM6Wpls7uwCvOWKWJ3OSojNG7OZezlwjns8oFN7sLqnZjQrxymj5p8C91tmBJZB3a8st43lGKhxIIVwjWmfHv5d6vFPU7Q+xsbImoAHOocxl0PhvBTMhBNjfPHhG5Pl+vuxoGPfnEP0jVh4g9GSu1jiq6N2m/q6F5tqWDpsYZXQ7fnYs6NcOcXLvaDQNaoAMvoVMfM+2b3ZPvyS4ozGIzAZDluYmFUDueepJ3x2xc78qVjKoXNWpF8XIBEWGeSVwTO45WvYF04pD/56WTY8gjIookbFGDHGyn11goIqRuzdnkbWNNmSQOX0mFP1gXRF+mZ5q1D5dUyotLS3EFJ6foeI8D/0CkMeZqnKl4JYLqBmmL2A9YIui94X2OuPVOuk+6Zo9RtoyGm14XcQbNfUG8ToK1sqyvXdogyw513Erfq2vLI1phE7Oy1c6rIijIAFjBVjWmESllOPBlHUFoe/VZ48UJyy7dw6M= p.jarosz@vipower.pl"

echo $SSH_RSA_KEY_MADACH >> ~/.ssh/authorized_keys
echo $SSH_RSA_KEY_PJAROSZ >> ~/.ssh/authorized_keys

chmod 600 ~/.ssh/authorized_keys