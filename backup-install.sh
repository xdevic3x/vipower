#!/bin/bash

mkdir -p /root/scripts/
cd /root/scripts/
MACHINE_TYPE=`uname -m`

if [ ${MACHINE_TYPE} == 'x86_64' ]; then
  wget --quiet https://github.com/borgbackup/borg/releases/download/1.1.10/borg-linux64 -O /usr/local/bin/borg && chmod +x /usr/local/bin/borg
else
  wget --quiet https://github.com/borgbackup/borg/releases/download/1.1.10/borg-linux32 -O /usr/local/bin/borg && chmod +x /usr/local/bin/borg
fi

wget --quiet https://bitbucket.org/vibiznes/vipower/raw/master/borg-backup.sh -O /root/scripts/borg-backup.sh && chmod +x /root/scripts/borg-backup.sh
wget --quiet https://bitbucket.org/vibiznes/vipower/raw/master/variables-backup.sh -O /root/scripts/variables-backup.sh && chmod +x /root/scripts/variables-backup.sh
wget --quiet https://bitbucket.org/vibiznes/vipower/raw/master/mysql-ssh-backup.sh -O /root/scripts/mysql-ssh-backup.sh && chmod +x /root/scripts/mysql-ssh-backup.sh
wget --quiet https://bitbucket.org/vibiznes/vipower/raw/master/backup-list.sh -O /root/scripts/backup-list.sh && chmod +x /root/scripts/backup-list.sh
wget --quiet https://bitbucket.org/vibiznes/vipower/raw/master/backup/da-backup.sh -O /root/scripts/da-backup.sh && chmod +x /root/scripts/da-backup.sh
wget --quiet https://bitbucket.org/vibiznes/vipower/raw/master/backup/cp-backup.sh -O /root/scripts/cp-backup.sh && chmod +x /root/scripts/cp-backup.sh

if [ ! -f /root/scripts/hostname ]; then
    echo "Set hostname in /root/scripts/hostname !" >> /root/scripts/hostname
fi

if [ ! -f /root/scripts/borg-backup-pre.sh ]; then
tee /root/scripts/borg-backup-pre.sh << END >/dev/null
#!/bin/bash
source /root/scripts/variables-backup.sh

#/root/scripts/da-backup.sh $1
#/root/scripts/cp-backup.sh $1
END
chmod +x /root/scripts/borg-backup-pre.sh
fi

echo "Done"
