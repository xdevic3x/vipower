#!/bin/bash
source /root/scripts/variables-backup.sh
BACKUPDIR2="~/postgres"

#postgresql
$SSHBIN -o "StrictHostKeyChecking=no" -o "UserKnownHostsFile=/dev/null" $SSH 'mkdir -p '$BACKUPDIR2'/'$DATA''
rm -rf $OUTPUT
echo > $BACKUPLOG
mkdir $OUTPUT
chmod 0777 $OUTPUT
for db in $(su - postgres -c 'psql -q -A -t -c "SELECT datname FROM pg_database"'); do
        start=$(date +%s)
        su - postgres -c 'pg_dump '$db' | gzip > '$OUTPUT'/'$db'.sql.gz'
        $SCPBIN -o "StrictHostKeyChecking=no" -o "UserKnownHostsFile=/dev/null" $OUTPUT/$db.sql.gz $SSH:$BACKUPDIR2/$DATA/$db.sql.gz
        stop=$(date +%s)
        echo "Dumping database: $db '$((stop-start))s'" >> $BACKUPLOG
        rm -f $OUTPUT/$db.sql.gz
done

backupsize=$($SSHBIN -o "StrictHostKeyChecking=no" -o "UserKnownHostsFile=/dev/null" $SSH 'du -sh '$BACKUPDIR2'/'$DATA'')
sed -i '1 i\ Backup size: '"$backupsize"'' $BACKUPLOG
subject="PgSQL Backup - '$HOST'" message=$(cat $BACKUPLOG) send_mail

#fix perms
$SSHBIN -o "StrictHostKeyChecking=no" -o "UserKnownHostsFile=/dev/null" $SSH 'find '$BACKUPDIR2' -type d -print0 |xargs -0 chmod 700'
$SSHBIN -o "StrictHostKeyChecking=no" -o "UserKnownHostsFile=/dev/null" $SSH 'find '$BACKUPDIR2' -type f -print0 |xargs -0 chmod 600'

#clear old backups
$SSHBIN -o "StrictHostKeyChecking=no" -o "UserKnownHostsFile=/dev/null" $SSH 'find '$BACKUPDIR2' -mtime +'$BACKUPTIME' -type d -print0 |xargs -0 rm -rf'