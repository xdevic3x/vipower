#!/bin/bash

#fix cpanel mountpoint
mount -o remount /tmp

CP_BACKUP_LOG='/dev/null'

source /root/scripts/variables-backup.sh
mkdir -p /home/nodea_backups/
rm -rf /home/nodea_backups/
mkdir -p /home/nodea_backups/
for user in $(ls /var/cpanel/users); do
    /scripts/pkgacct --skiphomedir --skiplogs --skipmysql --skippgsql $user /home/nodea_backups/ >> $CP_BACKUP_LOG
done